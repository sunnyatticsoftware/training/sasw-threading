﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Sasw.Threading.TaskBasic
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = new System.Threading.Tasks.Task(SimpleMethod);
            task.Start();

            var taskWithReturn = new Task<string>(MethodWithReturn);
            taskWithReturn.Start();
            taskWithReturn.Wait();
            Console.Write(taskWithReturn.Result);
        }

        private static string MethodWithReturn()
        {
            Thread.Sleep(2000);
            return "hello";
        }

        private static void SimpleMethod()
        {
            Console.WriteLine("Hello World");
        }
    }
}