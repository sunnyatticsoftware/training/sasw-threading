﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Sasw.Threading.TaskIO
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Factory.StartNew<string>(() => GetPosts("https://jsonplaceholder.typicode.com/posts"));
            SomethingElse();

            //task.Wait();
            
            try
            {
                // any exception when waiting would be wrapped in an aggregate exception
                // task.Result blocks execution until the task is completed because it waits for it, which is good but it can be dangerous if we don't use it properly
                Console.Write(task.Result); 
            }
            catch (AggregateException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void SomethingElse()
        {
            // dummy
        }

        private static string GetPosts(string url)
        {
            // ReSharper disable once PossibleNullReferenceException
            throw null; // this exception is not handled because it's in the wait
            using var client = new WebClient();
            return client.DownloadString(url);
        }
    }
}