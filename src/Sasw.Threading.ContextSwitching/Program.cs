﻿using System;
using System.Threading;

namespace Sasw.Threading.ContextSwitching
{
    class Program
    {
        static void Main(string[] args)
        {
            var thread = new Thread(
                () =>
                {
                    for (var i = 0; i <= 1000; i++)
                    {
                        Console.Write($"W {i} ");
                    }
                });
            // Worker thread
            thread.Name = "Sasw - W";
            thread.Start();
            
            // Main thread
            Thread.CurrentThread.Name = "Sasw - M";
            for (var i = 0; i <= 1000; i++)
            {
                Console.Write($"M {i} ");
            }
        }
    }
}