﻿using System;
using System.Threading;

namespace Sasw.Threading.LocalMemory
{
    class Program
    {
        static void Main(string[] args)
        {
            // Worker thread
            new Thread(PrintOneToThirty).Start();
            // Main thread
            PrintOneToThirty();
        }

        private static void PrintOneToThirty()
        {
            for (var i = 0; i < 30; i++)
            {
                // i is in local memory. Each thread has its own
                Console.WriteLine($"{i + 1} ");
            }
        }
    }
}