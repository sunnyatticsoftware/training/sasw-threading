﻿using System;
using System.Threading;

namespace Sasw.Threading.Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Demo();
        }

        private static void Demo()
        {
            new Thread(Execute).Start();
        }

        static void Execute()
        {
            // the solution is to catch exception in thread where it happens
            try
            {
                // ReSharper disable once PossibleNullReferenceException
                throw null; // to have a null pointer exception
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}