﻿using System;
using System.Threading;

namespace Sasw.Threading.ThreadPool
{
    class Program
    {
        static void Main(string[] args)
        {
            var employee =
                new Employee
                {
                    Name = "Diego",
                    CompanyName = "Sasw"
                };
            System.Threading.ThreadPool.QueueUserWorkItem(
                new WaitCallback(DisplayEmployee), employee);

            // This is now redundant
            var processorCount = Environment.ProcessorCount;
            System.Threading.ThreadPool.SetMaxThreads(processorCount * 2, processorCount * 2);
            Console.WriteLine($"Processor count: {processorCount}");

            var workerThreads = 0;
            var completionPortThreads = 0;
            System.Threading.ThreadPool.GetMinThreads(out workerThreads, out completionPortThreads);
            System.Threading.ThreadPool.SetMaxThreads(workerThreads * 2, completionPortThreads * 2);
            
            Console.WriteLine($"Is thread pool in Main: {Thread.CurrentThread.IsThreadPoolThread}");
            
            
            
            Console.ReadKey();
        }

        private static void DisplayEmployee(object employee)
        {
            Console.WriteLine($"Is thread pool in DisplayEmployee: {Thread.CurrentThread.IsThreadPoolThread}");
            var emp = employee as Employee;
            Console.WriteLine($"Person name is {emp.Name} and company {emp.CompanyName}");
        }
    }

    public class Employee
    {
        public string Name { get; set; }
        public string CompanyName { get; set; }
    }
}