﻿using System;
using System.Threading.Tasks;

namespace Sasw.Threading.TaskContinuation
{
    class Program
    {
        static void Main(string[] args)
        {
            var antecedent = Task.Run(() =>
            {
                Task.Delay(2000).Wait(); // not using thread.sleep but task delay. WHY it does not work???
                return DateTime.Today.ToShortDateString();
            });
            var continuation = antecedent.ContinueWith(x =>
            {
                return $"Today is {antecedent.Result}";
            });
            Console.WriteLine("This will display before the result");
            Console.WriteLine(continuation.Result);
        }
    }
}