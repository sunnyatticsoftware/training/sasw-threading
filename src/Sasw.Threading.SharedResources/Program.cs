﻿using System;
using System.Threading;

namespace Sasw.Threading.SharedResources
{
    class Program
    {
        private static bool _isCompleted;
        private static readonly object lockCompleted = new object();

        static void Main(string[] args)
        {
            var thread = new Thread(HelloWorld);
            // Worker thread
            thread.Start();
            // Main thread
            HelloWorld();
        }

        private static void HelloWorld()
        {
            // As there is a shared resurce, a lock is placed (semaphore) so that
            // only one thread can access the shared resource at once
            lock (lockCompleted)
            {
                if (!_isCompleted)
                {
                    Console.WriteLine("Hello World should print only once");
                    _isCompleted = true;
                }
            }
        }
    }
}